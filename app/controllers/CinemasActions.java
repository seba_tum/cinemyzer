

package controllers;

/**
 * Created by Hazem on 03/06/15.
 */
import models.Cinema;
import models.Movie;
import models.Show;

import play.api.Play;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.data.*;
import play.mvc.*;
import java.util.Date;
import java.util.List;


public class CinemasActions extends Controller {

    @play.db.jpa.Transactional
    public static Result createCinema()
    {
        Cinema cinema = Form.form(Cinema.class).bindFromRequest().get();

        if(Form.form().bindFromRequest().get("groupDiscount")==null) {
            cinema.groupDiscount = 0;
        }
        else {
            cinema.groupDiscount = 1;
        }
        cinema.numberOfRooms=Integer.parseInt( Form.form().bindFromRequest().get("numberOfRooms"));
        cinema.createCinema();
        return redirect(controllers.routes.CinemasActions.addNewCinema());

    }
    @play.db.jpa.Transactional
    public static Result addNewCinema() {
        List<Cinema>  allCinemas=Cinema.getAllCinemas();
        return ok(views.html.cinemas.render(allCinemas));
    }

    @play.db.jpa.Transactional(readOnly = true)
    public static Result showAll()
    {
        List<Cinema>  allCinemas=Cinema.getAllCinemas();
        return ok(views.html.cinemas.render(allCinemas));
    }

}
