package controllers;

import play.mvc.Controller;
import play.mvc.Result;

/**
 * Created by Hazem on 03/06/15.
 */
public class SearchActions extends Controller {
    public static Result index() {
        return ok(views.html.search.render());

    }
}
