package controllers;

import models.Movie;
import play.mvc.Controller;
import play.mvc.Result;
import java.util.List;


/**
 * Created by Hazem on 03/06/15.
 */
public class MoviesActions extends Controller {

    @play.db.jpa.Transactional(readOnly = true)
    public static Result showMovies()
    {
        List<Movie> allMovies= Movie.getAllMovies();
        return ok(views.html.movies.render(allMovies));
    }
}
