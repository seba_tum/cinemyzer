package controllers;

/**
 * Created by Hazem on 01/06/15.
 */

//import com.sun.xml.internal.rngom.parse.host.ScopeHost;
import models.Cinema;
import models.Movie;
import models.Show;

import play.api.Play;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.data.*;
import play.mvc.*;
import java.util.Date;
import java.util.List;



public class ShowsActions extends Controller {

    @play.db.jpa.Transactional
    public static Result createShow()
    {

        Show show = Form.form(Show.class).bindFromRequest().get();
        if(Form.form().bindFromRequest().get("originalVersion")==null) {
            show.originalVersion = 0;
        }
        else {
            show.originalVersion = 1;
        }
        if(Form.form().bindFromRequest().get("is3D")==null) {
            show.is3D = 0;
        }
        else {
            show.is3D = 1;
        }
        show.ticketPrice=Integer.parseInt( Form.form().bindFromRequest().get("ticketPrice"));
        show.capacity=Integer.parseInt( Form.form().bindFromRequest().get("capacity"));
        if(Form.form().bindFromRequest().get("cinemaId")!=null)
        {
            show.cinemaId=Integer.parseInt( Form.form().bindFromRequest().get("cinemaId"));
        }
        if(Form.form().bindFromRequest().get("movieId")!=null)
        {
            show.movieId=Integer.parseInt( Form.form().bindFromRequest().get("movieId"));
        }
        show.createShow();
        return redirect(controllers.routes.ShowsActions.addNewShow());
    }

    @play.db.jpa.Transactional
    public static Result addNewShow() {
        List<Movie>  allMovies=Movie.getAllMovies();
        List<Cinema>  allCinemas=Cinema.getAllCinemas();
        List<Show>  allShows=Show.getAllShows();
        return ok(views.html.shows.render(allCinemas,allMovies,allShows));
    }

    @play.db.jpa.Transactional(readOnly = true)
    public static Result showAll()
    {
        List<Movie>  allMovies=Movie.getAllMovies();
        List<Cinema>  allCinemas=Cinema.getAllCinemas();
        List<Show>  allShows=Show.getAllShows();
        return ok(views.html.shows.render(allCinemas,allMovies,allShows));
    }

}
