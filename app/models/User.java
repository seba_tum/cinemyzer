package models;

import com.sun.org.apache.xpath.internal.operations.Bool;
import play.db.jpa.JPA;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Hazem on 01/06/15.
 */
@Entity
public class User {
    @Id
    @GeneratedValue
    protected int id;
    private String name;
    private String password;
    private String email;
    private String userName;
    private int age;
    private boolean isStudent;
    private byte[] image;
    private int loyalty;
    private boolean isCommnetsAnnonimity;
    private int typeOfUser;
    //private Event[] events;
    //private SearchCriteria[] searchCriterias;
    //private Feedback[] feedbacks;

    public User(String name,String password,String email,String userName,int age,
                boolean isStudent,byte[] image, int loyalty,boolean isCommnetsAnnonimity,int typeOfUser)
    {
        this.name=name;
        this.password=password;
        this.email=email;
        this.userName=userName;
        this.age=age;
        this.isStudent=isStudent;
        this.image=image;
        this.loyalty=loyalty;
        this.isCommnetsAnnonimity=isCommnetsAnnonimity;
        this.typeOfUser=typeOfUser;
    }

    public void createUser()
    {
        JPA.em().persist(this);
    }
    public boolean login()
    {
        return false;
    }

    public boolean logout()
    {
        return false;
    }

    public boolean register()
    {
        return false;
    }

    public boolean deleteAccount()
    {
        return false;
    }

    public boolean subscribe()
    {
        return false;
    }
    public boolean unsubscribe()
    {
        return false;
    }
}
