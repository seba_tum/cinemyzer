package models;


import play.db.jpa.JPA;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.Constraint;
import java.util.Date;

/**
 * Created by Hazem on 01/06/15.
 */

@Entity
public class SystemPredefinedValue {
    @Id
    @GeneratedValue
    protected int id;
}
