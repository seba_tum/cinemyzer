package models;


import play.db.jpa.JPA;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.Constraint;
import java.util.Date;

/**
 * Created by Hazem on 01/06/15.
 */

@Entity
public class SearchCriteria {
    @Id
    @GeneratedValue
    protected int id;

    public void saveUserSearchFieldValue()
    {

    }

    public void executeQuery()
    {

    }

    public void changeResultsView()
    {

    }
}
