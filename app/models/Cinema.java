package models;


import play.db.jpa.JPA;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.TypedQuery;
import javax.validation.Constraint;
import java.util.Date;
import java.util.List;

/**
 * Created by Hazem on 01/06/15.
 */

@Entity
public class Cinema {
    @Id
    @GeneratedValue
    public int id;
    public String location;
    public String name;
    public int numberOfRooms;
    public double rating;
    public int groupDiscount;
    public String cinemaURL;
//    private Feedback[] feedbacks;
//    private Show[] shows;


    public Cinema()
    {}
    public void createCinema()
    {
        JPA.em().persist(this);
    }

    public static List<Cinema> getAllCinemas() {
        TypedQuery<Cinema> query = JPA.em().createQuery("SELECT m FROM Cinema m", Cinema.class);
        return query.getResultList();
    }

    public static Cinema findById(Integer id) {
        return JPA.em().find(Cinema.class, id);
    }
    public void editCinema()
    {

    }
    public void deleteCinema()
    {

    }
}
