package models;


import play.db.jpa.JPA;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.TypedQuery;
import javax.validation.Constraint;
import java.util.Date;
import java.util.List;

/**
 * Created by Hazem on 01/06/15.
 */

@Entity
public class Movie {
    @Id
    @GeneratedValue
    public int id;
    public String imdbId;
    public String title;
   // private Feedback[] feedbacks;
    public Movie()
    {

    }

    public static Movie findById(Integer id) {
        return JPA.em().find(Movie.class, id);
    }

    public static List<Movie> getAllMovies() {
        TypedQuery<Movie> query = JPA.em().createQuery("SELECT m FROM Movie m", Movie.class);
        return query.getResultList();
    }
}

