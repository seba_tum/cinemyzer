package models;

import com.google.common.annotations.VisibleForTesting;

import org.hibernate.annotations.GeneratorType;
import play.data.validation.Constraints;
import play.db.jpa.*;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.*;

/**
 * Created by Hazem on 01/06/15.
 */

@Entity
public class Show {

    @Id
    @GeneratedValue
    protected int id;
    public String startTime;
    public String showLanguage;
    public int originalVersion;
    public int is3D;
    public int capacity;
    public double ticketPrice;
    public int movieId;
    public int cinemaId;


    public Show()
    {

    }


    public void createShow()
    {
        JPA.em().persist(this);
    }

    public static List<Show> getAllShows() {
        TypedQuery<Show> query = JPA.em().createQuery("SELECT m FROM Show m", Show.class);
        return query.getResultList();
    }

    public boolean deleteShow()
    {
        return false;
    }

    public  boolean updateShow()
    {
        return false;
    }

}
