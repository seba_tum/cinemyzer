CREATE TABLE `User` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`name`	TEXT NOT NULL,
	`password`	TEXT NOT NULL,
	`email`	TEXT NOT NULL UNIQUE,
	`userName`	TEXT NOT NULL UNIQUE,
	`age`	INTEGER NOT NULL,
	`student`	INTEGER,
	`profilePicture`	BLOB,
	`loyalty`	INTEGER,
	`commentsAnnonimity`	INTEGER,
	`typeOfUser`	INTEGER NOT NULL
);